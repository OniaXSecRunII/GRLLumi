from rootpy.io import root_open
from rootpy.plotting import Hist2D
from goodruns import GRL

f = root_open('data15.root')
h = f.run_vs_lumiblock

grl = GRL()

for i in h:
    if(i.value):
        print(i)
        x, y, z = i.xyz
        grl.insert(x, (y,y))

grl.save('GRL_data15.xml')
