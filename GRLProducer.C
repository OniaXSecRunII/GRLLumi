#define GRLProducer_cxx
// The class definition in GRLProducer.h has been generated automatically
// by the ROOT utility TTree::MakeSelector(). This class is derived
// from the ROOT class TSelector. For more information on the TSelector
// framework see $ROOTSYS/README/README.SELECTOR or the ROOT User Manual.


// The following methods are defined in this file:
//    Begin():        called every time a loop on the tree starts,
//                    a convenient place to create your histograms.
//    SlaveBegin():   called after Begin(), when on PROOF called only on the
//                    slave servers.
//    Process():      called for each event, in this function you decide what
//                    to read and fill your histograms.
//    SlaveTerminate: called at the end of the loop on the tree, when on PROOF
//                    called only on the slave servers.
//    Terminate():    called at the end of the loop on the tree,
//                    a convenient place to draw/fit your histograms.
//
// To use this file, try the following session on your Tree T:
//
// root> T->Process("GRLProducer.C")
// root> T->Process("GRLProducer.C","some options")
// root> T->Process("GRLProducer.C+")
//


#include "GRLProducer.h"
#include <TH2.h>
#include <TStyle.h>

void GRLProducer::Begin(TTree * /*tree*/)
{
	// The Begin() function is called at the start of the query.
	// When running with PROOF Begin() is only called on the client.
	// The tree argument is deprecated (on PROOF 0 is passed).

	TString option = GetOption();

	Info("Begin", "Starting GRLProducer with process option: %s", option.Data());

}

void GRLProducer::SlaveBegin(TTree * /*tree*/)
{
	// The SlaveBegin() function is called after the Begin() function.
	// When running with PROOF SlaveBegin() is called on each slave server.
	// The tree argument is deprecated (on PROOF 0 is passed).

	TString option = GetOption();

	int x_hi, x_low, x_nbins;
	if (option.Contains("data15")){
		x_low = 251101; x_hi = 287931;
	}
	else if (option.Contains("data16")){
		x_low = 289496; x_hi = 321044;
	}
	else if (option.Contains("data26")){
		x_low = 289496; x_hi = 321044;
	}
	else if (option.Contains("data17")){
		x_low = 316677; x_hi = 342182;
	}

	x_nbins = x_hi - x_low;

	//f_h2F = new TH2F("run_vs_lumiblock", "", 100000, 250000, 350000, 5000, 0, 5000);
	f_h2F = new TH2F("run_vs_lumiblock", "", x_nbins, x_low, x_hi, 5000, 0, 5000);
	fOutput->Add(f_h2F);

}

Bool_t GRLProducer::Process(Long64_t entry)
{
	// The Process() function is called for each entry in the tree (or possibly
	// keyed object in the case of PROOF) to be processed. The entry argument
	// specifies which entry in the currently loaded tree is to be processed.
	// When processing keyed objects with PROOF, the object is already loaded
	// and is available via the fObject pointer.
	//
	// This function should contain the \"body\" of the analysis. It can contain
	// simple or elaborate selection criteria, run algorithms on the data
	// of the event and typically fill histograms.
	//
	// The processing can be stopped by calling Abort().
	//
	// Use fStatus to set the return value of TTree::Process().
	//
	// The return value is currently not used.

	fReader.SetEntry(entry);

	f_h2F->Fill(*RunNumber,*LumiBlock);

	return kTRUE;
}

void GRLProducer::SlaveTerminate()
{
	// The SlaveTerminate() function is called after all entries or objects
	// have been processed. When running with PROOF SlaveTerminate() is called
	// on each slave server.

}

void GRLProducer::Terminate()
{
	// The Terminate() function is the last function to be called during
	// a query. It always runs on the client, it can be used to present
	// the results graphically or save the results to file.

	TString option = GetOption();

	TString outName;

	if (option.Contains("data15")){
		outName = "data15.root";
	}
	else if (option.Contains("data16")){
		outName = "data16.root";
	}
	else if (option.Contains("data26")){
		outName = "data26.root";
	}
	else if (option.Contains("data17")){
		outName = "data17.root";
	}

	f_h2F = (TH2F*)(fOutput->FindObject("run_vs_lumiblock"));
	TFile *f = new TFile(outName,"RECREATE");
	f_h2F->Write();
	f->Close();

}
