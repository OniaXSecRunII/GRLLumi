#!/bin/bash

#Script to calculate prescaled luminosity for triggers needed for run2
#inclusive J/psi xsection measurement
rename_move()
{
  for FILE in `ls *.root`;do
    mv $FILE ./Lumifiles/"Lumifile_"$1_$2".root"
    mv *.xml ./Lumifiles/
    mv *.tmp ./Lumifiles/
  done
}
#***********************************************
TRIGGER_2015_2MU=HLT_2mu4_bJpsimumu
TRIGGER_2015_1MU=HLT_mu50

TRIGGER_2016_2MU_MAIN=HLT_mu6_mu4_bJpsimumu
TRIGGER_2016_2MU_DELY=HLT_mu6_mu4_bJpsimumu_delayed #for 302956 and after
TRIGGER_2016_1MU=HLT_mu50

TRIGGER_2017_2MU=HLT_2mu10_bJpsimumu
TRIGGER_2017_1MU=HLT_mu50

TRIGGER_2018_1MU=HLT_mu50

LUMITAG_2015=OflLumi-13TeV-010
LUMITAG_2016=OflLumi-13TeV-010
LUMITAG_2017=OflLumi-13TeV-010
LUMITAG_2018=OflLumi-13TeV-001

LIVEFRAC_2015=L1_EM12
LIVEFRAC_2016=L1_EM12
LIVEFRAC_2017=L1_EM24VHI
LIVEFRAC_2018=L1_EM24VHI

LARTAG_2015=LARBadChannelsOflEventVeto-RUN2-UPD4-04
LARTAG_2016=LARBadChannelsOflEventVeto-RUN2-UPD4-04
LARTAG_2017=LARBadChannelsOflEventVeto-RUN2-UPD4-08
LARTAG_2018=LARBadChannelsOflEventVeto-RUN2-UPD4-10
#***********************************************Setup ATHENA release
setupATLAS
lsetup 'asetup AtlasProduction,20.7.8.6,here'
#***********************************************Checkout and build LumiCalc
if [[ ! -d LumiBlock ]]; then
  cmt co -r LumiCalc-00-04-32 LumiBlock/LumiCalc #adjust version to latest at the bottom of https://atlas-lumicalc.cern.ch/
  cd LumiBlock/LumiCalc/cmt/
  gmake
  cd -
fi
#***********************************************Make output directory
mkdir -p Lumifiles
#***********************************************Check if LumiCalc is available
hash iLumiCalc.exe
if [[ $? != 0 ]]; then
  echo "make_lumi_files.sh::LumiCalc is not available, package was not build successfully?"
  kill -INT $$
fi
#***********************************************Produce Lumi files.
#WARNING:Make sure you are using the LATEST GRLs.
#***********************************************
DATA_PATH="/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/"
#GRL_2015=$DATA_PATH"/data15_13TeV/20160720/data15_13TeV.periodAllYear_DetStatus-v79-repro20-02_DQDefects-00-02-02_PHYS_StandardGRL_All_Good_25ns.xml"
GRL_2015="/afs/cern.ch/work/b/bchargei/private/JPsi/rel21/Analysis/OniaAna/OniaAna/data/data15_13TeV.periodAllYear_DetStatus-v89-pro21-02_Unknown_PHYS_StandardGRL_All_Good_25ns.xml"
GRL_2016="/afs/cern.ch/work/b/bchargei/private/JPsi/rel21/Analysis/OniaAna/OniaAna/data/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml"
GRL_2017="/afs/cern.ch/work/b/bchargei/private/JPsi/rel21/Analysis/OniaAna/OniaAna/data/data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml"
GRL_2018="/afs/cern.ch/work/b/bchargei/private/JPsi/rel21/Analysis/OniaAna/OniaAna/data/data18_13TeV.periodAllYear_DetStatus-v102-pro22-04_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml"
##********************2015
#iLumiCalc.exe --lumitag=$LUMITAG_2015 --livetrigger=$LIVEFRAC_2015 --trigger=$TRIGGER_2015_2MU --xml=$GRL_2015 --lar --lartag=$LARTAG_2015 --plots
#rename_move 2015 $TRIGGER_2015_2MU
#
#iLumiCalc.exe --lumitag=$LUMITAG_2015 --livetrigger=$LIVEFRAC_2015 --trigger=$TRIGGER_2015_1MU --xml=$GRL_2015 --lar --lartag=$LARTAG_2015 --plots
#rename_move 2015 $TRIGGER_2015_1MU
##********************2016
#iLumiCalc.exe --lumitag=$LUMITAG_2016 --livetrigger=$LIVEFRAC_2016 --trigger=$TRIGGER_2016_2MU_MAIN --xml=$GRL_2016 --lar --lartag=$LARTAG_2016 --plots
#rename_move 2016 $TRIGGER_2016_2MU_MAIN
#
#iLumiCalc.exe --lumitag=$LUMITAG_2016 --livetrigger=$LIVEFRAC_2016 --trigger=$TRIGGER_2016_2MU_DELY --xml=$GRL_2016 --lar --lartag=$LARTAG_2016 --plots --run=302956-
#rename_move 2016 $TRIGGER_2016_2MU_DELY
#
#iLumiCalc.exe --lumitag=$LUMITAG_2016 --livetrigger=$LIVEFRAC_2016 --trigger=$TRIGGER_2016_1MU --xml=$GRL_2016 --lar --lartag=$LARTAG_2016 --plots
#rename_move 2016 $TRIGGER_2016_1MU
##********************2017
#iLumiCalc.exe --lumitag=$LUMITAG_2017 --livetrigger=$LIVEFRAC_2017 --trigger=$TRIGGER_2017_2MU --xml=$GRL_2017 --lar --lartag=$LARTAG_2017 --plots
#rename_move 2017 $TRIGGER_2017_2MU
#
#iLumiCalc.exe --lumitag=$LUMITAG_2017 --livetrigger=$LIVEFRAC_2017 --trigger=$TRIGGER_2017_1MU --xml=$GRL_2017 --lar --lartag=$LARTAG_2017 --plots
#rename_move 2017 $TRIGGER_2017_1MU
#********************2018
#iLumiCalc.exe --lumitag=$LUMITAG_2018 --livetrigger=$LIVEFRAC_2018 --trigger=$TRIGGER_2018_2MU --xml=$GRL_2018 --lar --lartag=$LARTAG_2018 --plots
#rename_move 2018 $TRIGGER_2018_2MU

iLumiCalc.exe --lumitag=$LUMITAG_2018 --livetrigger=$LIVEFRAC_2018 --trigger=$TRIGGER_2018_1MU --xml=$GRL_2018 --lar --lartag=$LARTAG_2018 --plots
rename_move 2018 $TRIGGER_2018_1MU
##********************
