//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Jan 14 22:38:58 2019 by ROOT version 6.14/04
// from TTree tree/myTree
// found on file: /eos/atlas/user/b/bchargei/BLS/data_BPHY1/data15p/user.bchargei.data15_13TeV.periodD.physics_Main.DAOD_BPHY1.12_01_2019.v01.root_MYSTREAM/user.bchargei.16717517.MYSTREAM._000001.root
//////////////////////////////////////////////////////////

#ifndef GRLProducer_h
#define GRLProducer_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TSelector.h>
#include <TTreeReader.h>
#include <TTreeReaderValue.h>
#include <TTreeReaderArray.h>

// Headers needed by this particular selector
#include <vector>



class GRLProducer : public TSelector {
	public :
		TH2F   *f_h2F = 0;             //! Output histogram

		TTreeReader     fReader;  //!the tree reader
		TTree          *fChain = 0;   //!pointer to the analyzed TTree or TChain

		// Readers to access the data (delete the ones you do not need).
		TTreeReaderValue<Int_t> RunNumber = {fReader, "RunNumber"};
		TTreeReaderValue<Int_t> EventNumber = {fReader, "EventNumber"};
		TTreeReaderValue<Int_t> LumiBlock = {fReader, "LumiBlock"};
		TTreeReaderValue<Int_t> mcChannelNumber = {fReader, "mcChannelNumber"};
		TTreeReaderValue<Int_t> EventTrigger = {fReader, "EventTrigger"};
		TTreeReaderArray<float> PS = {fReader, "PS"};
		TTreeReaderArray<float> Truth_Parent_PdgId = {fReader, "Truth_Parent_PdgId"};
		TTreeReaderArray<float> Truth_Parent_Pt = {fReader, "Truth_Parent_Pt"};
		TTreeReaderArray<float> Truth_Parent_Eta = {fReader, "Truth_Parent_Eta"};
		TTreeReaderArray<float> Truth_Parent_Phi = {fReader, "Truth_Parent_Phi"};
		TTreeReaderArray<float> Truth_Parent_Mass = {fReader, "Truth_Parent_Mass"};
		TTreeReaderArray<float> Truth_Onia_PdgId = {fReader, "Truth_Onia_PdgId"};
		TTreeReaderArray<float> Truth_Onia_Pt = {fReader, "Truth_Onia_Pt"};
		TTreeReaderArray<float> Truth_Onia_Eta = {fReader, "Truth_Onia_Eta"};
		TTreeReaderArray<float> Truth_Onia_Phi = {fReader, "Truth_Onia_Phi"};
		TTreeReaderArray<float> Truth_Onia_Mass = {fReader, "Truth_Onia_Mass"};
		TTreeReaderArray<float> Truth_Onia_Vertex_X = {fReader, "Truth_Onia_Vertex_X"};
		TTreeReaderArray<float> Truth_Onia_Vertex_Y = {fReader, "Truth_Onia_Vertex_Y"};
		TTreeReaderArray<float> Truth_Onia_Vertex_Z = {fReader, "Truth_Onia_Vertex_Z"};
		TTreeReaderArray<float> Truth_MuMinus_Pt = {fReader, "Truth_MuMinus_Pt"};
		TTreeReaderArray<float> Truth_MuMinus_Eta = {fReader, "Truth_MuMinus_Eta"};
		TTreeReaderArray<float> Truth_MuMinus_Phi = {fReader, "Truth_MuMinus_Phi"};
		TTreeReaderArray<float> Truth_MuPlus_Pt = {fReader, "Truth_MuPlus_Pt"};
		TTreeReaderArray<float> Truth_MuPlus_Eta = {fReader, "Truth_MuPlus_Eta"};
		TTreeReaderArray<float> Truth_MuPlus_Phi = {fReader, "Truth_MuPlus_Phi"};
		TTreeReaderValue<Float_t> Truth_PV_Z = {fReader, "Truth_PV_Z"};
		TTreeReaderValue<Float_t> Truth_PV_X = {fReader, "Truth_PV_X"};
		TTreeReaderValue<Float_t> Truth_PV_Y = {fReader, "Truth_PV_Y"};
		TTreeReaderValue<Float_t> PV_Z = {fReader, "PV_Z"};
		TTreeReaderValue<Float_t> PV_X = {fReader, "PV_X"};
		TTreeReaderValue<Float_t> PV_Y = {fReader, "PV_Y"};
		TTreeReaderArray<float> DiMuon_Mass = {fReader, "DiMuon_Mass"};
		TTreeReaderArray<float> DiMuon_MassErr = {fReader, "DiMuon_MassErr"};
		TTreeReaderArray<float> DiMuon_Pt = {fReader, "DiMuon_Pt"};
		TTreeReaderArray<float> DiMuon_Rapidity = {fReader, "DiMuon_Rapidity"};
		TTreeReaderArray<float> DiMuon_ChiSq = {fReader, "DiMuon_ChiSq"};
		TTreeReaderArray<float> DiMuon_Vertex_X = {fReader, "DiMuon_Vertex_X"};
		TTreeReaderArray<float> DiMuon_Vertex_Y = {fReader, "DiMuon_Vertex_Y"};
		TTreeReaderArray<float> DiMuon_Vertex_Z = {fReader, "DiMuon_Vertex_Z"};
		TTreeReaderArray<float> Tau_ConstMass_MaxSumPt2 = {fReader, "Tau_ConstMass_MaxSumPt2"};
		TTreeReaderArray<float> TauErr_ConstMass_MaxSumPt2 = {fReader, "TauErr_ConstMass_MaxSumPt2"};
		TTreeReaderArray<float> Tau_InvMass_MaxSumPt2 = {fReader, "Tau_InvMass_MaxSumPt2"};
		TTreeReaderArray<float> TauErr_InvMass_MaxSumPt2 = {fReader, "TauErr_InvMass_MaxSumPt2"};
		TTreeReaderArray<float> Tau_ConstMass_Min_A0 = {fReader, "Tau_ConstMass_Min_A0"};
		TTreeReaderArray<float> TauErr_ConstMass_Min_A0 = {fReader, "TauErr_ConstMass_Min_A0"};
		TTreeReaderArray<float> Tau_InvMass_Min_A0 = {fReader, "Tau_InvMass_Min_A0"};
		TTreeReaderArray<float> TauErr_InvMass_Min_A0 = {fReader, "TauErr_InvMass_Min_A0"};
		TTreeReaderArray<float> Tau_ConstMass_Min_Z0 = {fReader, "Tau_ConstMass_Min_Z0"};
		TTreeReaderArray<float> TauErr_ConstMass_Min_Z0 = {fReader, "TauErr_ConstMass_Min_Z0"};
		TTreeReaderArray<float> Tau_InvMass_Min_Z0 = {fReader, "Tau_InvMass_Min_Z0"};
		TTreeReaderArray<float> TauErr_InvMass_Min_Z0 = {fReader, "TauErr_InvMass_Min_Z0"};
		TTreeReaderArray<int> DiMuon_TriggMatch = {fReader, "DiMuon_TriggMatch"};
		TTreeReaderArray<float> Muon_Phi_hx = {fReader, "Muon_Phi_hx"};
		TTreeReaderArray<float> Muon_CosTheta_hx = {fReader, "Muon_CosTheta_hx"};
		TTreeReaderArray<float> Muon_Phi_hxOLD = {fReader, "Muon_Phi_hxOLD"};
		TTreeReaderArray<float> Muon_CosTheta_hxOLD = {fReader, "Muon_CosTheta_hxOLD"};
		TTreeReaderArray<float> Muon0_RefTrack_Pt = {fReader, "Muon0_RefTrack_Pt"};
		TTreeReaderArray<float> Muon0_RefTrack_Eta = {fReader, "Muon0_RefTrack_Eta"};
		TTreeReaderArray<float> Muon0_RefTrack_Phi = {fReader, "Muon0_RefTrack_Phi"};
		TTreeReaderArray<float> Muon0_RefTrack_Theta = {fReader, "Muon0_RefTrack_Theta"};
		TTreeReaderArray<float> Muon0_Track_Pt = {fReader, "Muon0_Track_Pt"};
		TTreeReaderArray<float> Muon0_Track_Eta = {fReader, "Muon0_Track_Eta"};
		TTreeReaderArray<float> Muon0_Track_Phi = {fReader, "Muon0_Track_Phi"};
		TTreeReaderArray<float> Muon0_Track_Theta = {fReader, "Muon0_Track_Theta"};
		TTreeReaderArray<float> Muon0_Track_d0 = {fReader, "Muon0_Track_d0"};
		TTreeReaderArray<float> Muon0_Track_z0 = {fReader, "Muon0_Track_z0"};
		TTreeReaderArray<float> Muon0_CBTrack_Pt = {fReader, "Muon0_CBTrack_Pt"};
		TTreeReaderArray<float> Muon0_CBTrack_Eta = {fReader, "Muon0_CBTrack_Eta"};
		TTreeReaderArray<float> Muon0_CBTrack_Phi = {fReader, "Muon0_CBTrack_Phi"};
		TTreeReaderArray<int> Muon0_Charge = {fReader, "Muon0_Charge"};
		TTreeReaderArray<int> Muon0_Quality = {fReader, "Muon0_Quality"};
		TTreeReaderArray<int> Muon0_TriggMatch = {fReader, "Muon0_TriggMatch"};
		TTreeReaderArray<float> Muon1_RefTrack_Pt = {fReader, "Muon1_RefTrack_Pt"};
		TTreeReaderArray<float> Muon1_RefTrack_Eta = {fReader, "Muon1_RefTrack_Eta"};
		TTreeReaderArray<float> Muon1_RefTrack_Phi = {fReader, "Muon1_RefTrack_Phi"};
		TTreeReaderArray<float> Muon1_RefTrack_Theta = {fReader, "Muon1_RefTrack_Theta"};
		TTreeReaderArray<float> Muon1_Track_Pt = {fReader, "Muon1_Track_Pt"};
		TTreeReaderArray<float> Muon1_Track_Eta = {fReader, "Muon1_Track_Eta"};
		TTreeReaderArray<float> Muon1_Track_Phi = {fReader, "Muon1_Track_Phi"};
		TTreeReaderArray<float> Muon1_Track_Theta = {fReader, "Muon1_Track_Theta"};
		TTreeReaderArray<float> Muon1_Track_d0 = {fReader, "Muon1_Track_d0"};
		TTreeReaderArray<float> Muon1_Track_z0 = {fReader, "Muon1_Track_z0"};
		TTreeReaderArray<float> Muon1_CBTrack_Pt = {fReader, "Muon1_CBTrack_Pt"};
		TTreeReaderArray<float> Muon1_CBTrack_Eta = {fReader, "Muon1_CBTrack_Eta"};
		TTreeReaderArray<float> Muon1_CBTrack_Phi = {fReader, "Muon1_CBTrack_Phi"};
		TTreeReaderArray<int> Muon1_Charge = {fReader, "Muon1_Charge"};
		TTreeReaderArray<int> Muon1_Quality = {fReader, "Muon1_Quality"};
		TTreeReaderArray<int> Muon1_TriggMatch = {fReader, "Muon1_TriggMatch"};
		TTreeReaderArray<float> Muon0_RecoEffTight = {fReader, "Muon0_RecoEffTight"};
		TTreeReaderArray<float> Muon1_RecoEffTight = {fReader, "Muon1_RecoEffTight"};
		TTreeReaderArray<float> Muon0_RecoEffMedium = {fReader, "Muon0_RecoEffMedium"};
		TTreeReaderArray<float> Muon1_RecoEffMedium = {fReader, "Muon1_RecoEffMedium"};
		TTreeReaderArray<float> Muon0_RecoEffLoose = {fReader, "Muon0_RecoEffLoose"};
		TTreeReaderArray<float> Muon1_RecoEffLoose = {fReader, "Muon1_RecoEffLoose"};
		TTreeReaderArray<float> Muon0_RecoEffLowPt = {fReader, "Muon0_RecoEffLowPt"};
		TTreeReaderArray<float> Muon1_RecoEffLowPt = {fReader, "Muon1_RecoEffLowPt"};
		TTreeReaderArray<float> Muon0_mu50TriggEffTight = {fReader, "Muon0_mu50TriggEffTight"};
		TTreeReaderArray<float> Muon1_mu50TriggEffTight = {fReader, "Muon1_mu50TriggEffTight"};
		TTreeReaderArray<float> Muon0_mu50TriggEffMedium = {fReader, "Muon0_mu50TriggEffMedium"};
		TTreeReaderArray<float> Muon1_mu50TriggEffMedium = {fReader, "Muon1_mu50TriggEffMedium"};
		TTreeReaderArray<float> Muon1_mu50TriggEffLoose = {fReader, "Muon1_mu50TriggEffLoose"};
		TTreeReaderArray<float> Muon0_mu50TriggEffLoose = {fReader, "Muon0_mu50TriggEffLoose"};


		GRLProducer(TTree * /*tree*/ =0) { }
		virtual ~GRLProducer() { }
		virtual Int_t   Version() const { return 2; }
		virtual void    Begin(TTree *tree);
		virtual void    SlaveBegin(TTree *tree);
		virtual void    Init(TTree *tree);
		virtual Bool_t  Notify();
		virtual Bool_t  Process(Long64_t entry);
		virtual Int_t   GetEntry(Long64_t entry, Int_t getall = 0) { return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0; }
		virtual void    SetOption(const char *option) { fOption = option; }
		virtual void    SetObject(TObject *obj) { fObject = obj; }
		virtual void    SetInputList(TList *input) { fInput = input; }
		virtual TList  *GetOutputList() const { return fOutput; }
		virtual void    SlaveTerminate();
		virtual void    Terminate();

		ClassDef(GRLProducer,0);

};

#endif

#ifdef GRLProducer_cxx
void GRLProducer::Init(TTree *tree)
{
	// The Init() function is called when the selector needs to initialize
	// a new tree or chain. Typically here the reader is initialized.
	// It is normally not necessary to make changes to the generated
	// code, but the routine can be extended by the user if needed.
	// Init() will be called many times when running on PROOF
	// (once per file to be processed).

	fReader.SetTree(tree);
}

Bool_t GRLProducer::Notify()
{
	// The Notify() function is called when a new file is opened. This
	// can be either for a new TTree in a TChain or when when a new TTree
	// is started when using PROOF. It is normally not necessary to make changes
	// to the generated code, but the routine can be extended by the
	// user if needed. The return value is currently not used.

	return kTRUE;
}


#endif // #ifdef GRLProducer_cxx
